// EJ1, usuario ingresa nombre en menos de dos lineas 
function NameInput(){
    // Se consigue el nombre de la form y luego se guarda en "Nombre"
    var Nombre = document.formuser.Nombre.value;
    document.getElementById("texto").innerHTML = "Hola " + Nombre;
}

// EJ2, suma en menos de 3 lineas
function MathAdd(){
    // Se obtiene los 2 numeros que se ingresaron
    var num1 = document.formmath.firstnum.value;
    var num2 = document.formmath.secondnum.value;
    // Se muestran los resultados, las dos variables se multiplican por 1 para hacerlas variables de numeros
    document.getElementById("total").innerHTML = "Total = " + (num1*1 + num2*1);
}

// EJ3, ingresar 2 numeros y ver cual es mayor
function MathBiggie(){
    // Se obtiene los 2 numeros que se ingresaron
    var num1 = document.formmath.firstnum.value;
    var num2 = document.formmath.secondnum.value;
    // Se muestran los resultados, las dos variables se multiplican por 1 para hacerlas variables de numeros
    if (num1*1 > num2*1) {
        document.getElementById("total").innerHTML = "El numero mayor es: " + num1;
    }
    else if (num1*1 < num2*1) {
        document.getElementById("total").innerHTML = "El numero mayor es: " + num2;
    }
    else {
        document.getElementById("total").innerHTML = "Los numeros son iguales";
    }
}

// EJ, ingresar 3 numeros y ver cual es grande
function MathBiggieBut3(){
    // Se obtiene los 2 numeros que se ingresaron
    var num1 = document.formmath.firstnum.value;
    var num2 = document.formmath.secondnum.value;
    var num3 = document.formmath.thirdnum.value;
    // Se muestran los resultados, las dos variables se multiplican por 1 para hacerlas variables de numeros
    if ((num1*1 > num2*1) || (num1*1 == num2*1)) {
        if ((num1*1 > num3*1) || (num1*1 == num3*1)) {
            document.getElementById("total").innerHTML = "El numero mayor es: " + num1;
        }
        else{
            document.getElementById("total").innerHTML = "El numero mayor es: " + num3;
        }
    }
    else {
        if ((num2*1 > num3*1) || (num2*1 == num3*1)) {
            document.getElementById("total").innerHTML = "El numero mayor es: " + num2;
        }
        else{
            document.getElementById("total").innerHTML = "El numero mayor es: " + num3;
        }
    }
}

function DivisibleBy2(){
    var num1 = document.formmath.testnum.value;
    if ((num1*1)%2 == 0 ) {
        document.getElementById("total").innerHTML = "El numero es divisible por dos";
    }
    else{
        document.getElementById("total").innerHTML = "El numero no es divisible por dos";
    }
}

function CuentaAs(){
    // Se consigue el texto de la form, en donde se usará para contar las 'a's, se creara un valor fijo 0 por si no existe ninguna
    var TextoContar = document.formtext.Textocontar.value;
    var TOTAL = 0;
    // en el string se va a buscar todas las letras "a" y se contarán
    const confirm = TextoContar.match(/[a]/gi);
    if (confirm != null){
        const count = TextoContar.match(/[a]/gi).length;
        TOTAL = (TOTAL + count);
    }
    document.getElementById("Atotal").innerHTML = "El numero total de letras 'a' es:" + TOTAL;
}

function ConfirmaVocales(){
    // Se consigue el texto de la form, en donde se usará para confirmar las vocales
    var Texto = document.formtext.TextoVocal.value;
    // en el string se va a buscar todas las vocales por separado, si el valor no es nulo, deben existir en el texto
    const Acount = Texto.match(/[a]/gi);
    if(Acount != null){
        var Aconfirm = true;
    }
    else{
        var Aconfirm = false;
    }
    const Ecount = Texto.match(/[e]/gi);
    if(Ecount != null){
        var Econfirm = true;
    }
    else{
        var Econfirm = false;
    }
    const Icount = Texto.match(/[i]/gi);
    if(Icount != null){
        var Iconfirm = true;
    }
    else{
        var Iconfirm = false;
    }
    const Ocount = Texto.match(/[o]/gi);
    if(Ocount != null){
        var Oconfirm = true;
    }
    else{
        var Oconfirm = false;
    }
    const Ucount = Texto.match(/[u]/gi);
    if(Ucount != null){
        var Uconfirm = true;
    }
    else{
        var Uconfirm = false;
    }
    document.getElementById("Confirmar").innerHTML = "Vocales A: " + Aconfirm + " || " +
                                                     "Vocales E: " + Econfirm + " || " +
                                                     "Vocales I: " + Iconfirm + " || " +
                                                     "Vocales O: " + Oconfirm + " || " +
                                                     "Vocales U: " + Uconfirm;
}

function CuentaVocalesTotales(){
    // Se consigue el texto de la form, en donde se usará para contar las vocales
    var Texto = document.formtext.TextoVocal.value;
    // en el string se va a buscar todas las vocales por separado, si el valor no es nulo, deben existir en el texto
    // cuando existan, se va a realizar la suma de cuantos existes, caso contrario, se usará el valor default que es 0
    var aTotal = 0;
    var eTotal = 0;
    var iTotal = 0;
    var oTotal = 0;
    var uTotal = 0;
    const Aconf = Texto.match(/[a]/gi);
    if(Aconf != null){
        const acount = Texto.match(/[a]/gi).length;
        aTotal = (acount + aTotal);
    }
    const Econf = Texto.match(/[e]/gi);
    if(Econf != null){
        const ecount = Texto.match(/[e]/gi).length;
        eTotal = (ecount + eTotal);
    }
    const Iconf = Texto.match(/[i]/gi);
    if(Iconf != null){
        const icount = Texto.match(/[i]/gi).length;
        iTotal = (icount + iTotal);
    }
    const Oconf = Texto.match(/[o]/gi);
    if(Oconf != null){
        const ocount = Texto.match(/[o]/gi).length;
        oTotal = (ocount + oTotal);
    }
    const Uconf = Texto.match(/[u]/gi);
    if(Uconf != null){
        const ucount = Texto.match(/[u]/gi).length;
        uTotal = (ucount + uTotal);
    }
    document.getElementById("Confirmar").innerHTML = "Vocales A: " + aTotal + " || " +
                                                     "Vocales E: " + eTotal + " || " +
                                                     "Vocales I: " + iTotal + " || " +
                                                     "Vocales O: " + oTotal + " || " +
                                                     "Vocales U: " + uTotal;
}

function CLPtoUSD(){
    var Chilean = document.transcoin.CLP.value;
    // forumula clp a dolar
    var trans = (Chilean*1) * 0.0012;
    // se expresa los resultados
    document.getElementById("CLPVAL").innerHTML = Chilean;
    document.getElementById("USDVAL").innerHTML = trans;
}

function USDtoCLP(){
    var USD = document.transcoin.DOLLAR.value;
    // formula usd a clp
    var trans = (USD*1) * 854.70;
    // se expresa resultados
    document.getElementById("USDVAL").innerHTML = USD;
    document.getElementById("CLPVAL").innerHTML = trans;
}

function RTD(){
    // Se crea un numero al azar del 1 al 6 y con el resultando se consigue que dado será
    var Rolling = Math.floor(Math.random()*6) + 1;
    if(Rolling == 1 ){
        var Sauce = "img/Die1.png"
    }
    else if(Rolling == 2 ){
        var Sauce = "img/Die2.png"
        
    }
    else if(Rolling == 3 ){
        var Sauce = "img/Die3.png"
        
    }
    else if(Rolling == 4 ){
        var Sauce = "img/Die4.png"
        
    }
    else if(Rolling == 5 ){
        var Sauce = "img/Die5.png"
        
    }
    else if(Rolling == 6 ){
        var Sauce = "img/Die6.png"
        
    }
    // Se consigue el dado inicial para poder reemplazar la imagen, asi no se crean multiples con cada roll
    var olddie = document.getElementById("dado");
    // Se crea la variable de el dado el cual será crear la imagen
    var ElDado = document.createElement("img");
    ElDado.setAttribute("id", "dado")
    ElDado.setAttribute("src", Sauce);
    document.body.replaceChild(ElDado, olddie);
}

function NextCat(){
    // el alt representa la pagina donde está, con esto se guiará en que imagen está
    var ActualPage = document.getElementById("PresentCat").alt;
    // Dependiendo de la pagina, se colocará el siguiente gato
    if (ActualPage*1 < 5){
        ActualPage = ((ActualPage*1) + 1);
        if(ActualPage == 2){
            var Sauce = "img/dummy2.jpg";
        }
        else if(ActualPage ==3){
            var Sauce = "img/dummy3.jpg";
        }
        else if(ActualPage ==4){
            var Sauce = "img/dummy4.jpg";
        }
        else if(ActualPage ==5){
            var Sauce = "img/dummy5.jpg";
        }
        // El gato que estaba antes será categorizado como viejo para reemplazar
        var oldcat = document.getElementById("PresentCat");
        // Se crea la variable de el nuevo gato, donde el alt será la pagina nueva y se mostrará la nueva imagen gracias al if anterior
        var neocat = document.createElement("img");
        neocat.setAttribute("id", "PresentCat");
        neocat.setAttribute("src", Sauce);
        neocat.setAttribute("width", 650);
        neocat.setAttribute("height", 350);
        neocat.setAttribute("alt", ActualPage);
        document.body.replaceChild(neocat, oldcat);
    }
    document.getElementById("pagina").innerHTML = ActualPage;
}

function GiveCatAgain(){
        // el alt representa la pagina donde está, con esto se guiará en que imagen está
    var ActualPage = document.getElementById("PresentCat").alt;
    // Dependiendo de la pagina, se colocará el gato anterior
    if (ActualPage*1 > 1){
        ActualPage = ((ActualPage*1) - 1);
        if(ActualPage == 1){
            var Sauce = "img/dummy1.jpg";
        }
        else if(ActualPage ==2){
            var Sauce = "img/dummy2.jpg";
        }
        else if(ActualPage ==3){
            var Sauce = "img/dummy3.jpg";
        }
        else if(ActualPage ==4){
            var Sauce = "img/dummy4.jpg";
        }
        // El gato que estaba antes será categorizado como viejo para reemplazar
        var oldcat = document.getElementById("PresentCat");
        // Se crea la variable de el nuevo gato, donde el alt será la pagina nueva y se mostrará la nueva imagen gracias al if anterior
        var neocat = document.createElement("img");
        neocat.setAttribute("id", "PresentCat");
        neocat.setAttribute("src", Sauce);
        neocat.setAttribute("width", 650);
        neocat.setAttribute("height", 350);
        neocat.setAttribute("alt", ActualPage);
        document.body.replaceChild(neocat, oldcat);
    }
    document.getElementById("pagina").innerHTML = ActualPage;
}